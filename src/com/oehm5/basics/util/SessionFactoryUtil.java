package com.oehm5.basics.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil {

		private static SessionFactory sessionFactory = null;
		private SessionFactoryUtil() {
		}
		public static SessionFactory getSessionFactory() {
			
			if(sessionFactory == null)
				sessionFactory = new Configuration().configure().buildSessionFactory();
			
			return sessionFactory;
	}
}


