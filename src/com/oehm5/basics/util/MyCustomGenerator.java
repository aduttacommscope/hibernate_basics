package com.oehm5.basics.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class MyCustomGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		Connection connection = session.connection();
		try {
			Statement statement = connection.createStatement();

			ResultSet rs = statement.executeQuery("select count(sid) as 'Id' from studentdetails");

			if (rs.next()) {
				long sid = rs.getInt(1);
				System.out.println("sid : " + sid);
			    return sid;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
			return null;
	}
}
