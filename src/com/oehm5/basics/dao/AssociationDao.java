package com.oehm5.basics.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.oehm5.basics.dto.Brand;
import com.oehm5.basics.dto.Movie;
import com.oehm5.basics.dto.Team;
import com.oehm5.basics.util.SessionFactoryUtil;

public class AssociationDao {
	public void saveTeamDetails(Team team) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(team);
		transaction.commit();
	}
public void saveBrandDetails(Brand brand) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(brand);
		transaction.commit();

	}
public void saveBrandDetailsMto(Brand brandmto) {
	Session session = SessionFactoryUtil.getSessionFactory().openSession();
	Transaction transaction = session.beginTransaction();
	session.save(brandmto);
	transaction.commit();	
}
public void saveMovieDetails(Movie movie) {
	Session session = SessionFactoryUtil.getSessionFactory().openSession();
	Transaction transaction = session.beginTransaction();
	session.save(movie);
	transaction.commit();
}


}
