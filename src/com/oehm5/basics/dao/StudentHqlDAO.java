package com.oehm5.basics.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.oehm5.basics.dto.StudentDto;


public class StudentHqlDAO<student> {
	
	public List<student> getStudents() {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory  = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from StudentDto";
		Query query = session.createQuery(hql);
		List<student> list = query.list();
		return list;	
	}

	public StudentDto getStudentByContactNumber(Long contactNumber) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from StudentDto where contactNumber=:ContactNumber";
		Query query = session.createQuery(hql);
		query.setParameter("ContactNumber", contactNumber);
		StudentDto student = (StudentDto) query.uniqueResult();
		return student;
	}
	public void updateContactNumberById(Long sid, Long contactNumber) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="update StudentDto set contactNumber=:ContactNumber where sid=:id";
		Query query = session.createQuery(hql);
		query.setParameter("ContactNumber", contactNumber);
		query.setParameter("id", sid);
		int rowCount = query.executeUpdate();
		if(rowCount != 0) {
			System.out.println("Data updated successfully");
		}else {
			System.out.println("Update operation failed");
		}
	}
	public void deleteById(Long id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="delete from StudentDto where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int rowCount = query.executeUpdate();
		if(rowCount != 0) {
			System.out.println("Data deleted successfully");
		}else {
			System.out.println("Delete operation failed");
		}
	}

 
}
