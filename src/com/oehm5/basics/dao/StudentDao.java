package com.oehm5.basics.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.oehm5.basics.dto.StudentDto;
import com.oehm5.basics.util.SessionFactoryUtil;

public class StudentDao {
	public void savestudentdetails(StudentDto student)
	{
		/*
		 * Configuration configuration = new Configuration(); configuration.configure();
		 * SessionFactory sessionFactory = configuration.buildSessionFactory();
		 * configuration.addAnnotatedClass(StudentDao.class); Session session =
		 * sessionFactory.openSession(); Transaction transaction =
		 * session.beginTransaction();
		 */
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(student);
		transaction.commit();
		
	}
	
	public StudentDto getStudentById1(Long sid) {
		/*
		 * Configuration configuration = new Configuration(); configuration.configure();
		 * SessionFactory sessionFactory = configuration.buildSessionFactory(); Session
		 * session = sessionFactory.openSession(); StudentDto studentdto =
		 * session.get(StudentDto.class, sid);
		 */
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		StudentDto studentdto = session.get(StudentDto.class, sid);
		return studentdto;
	}
 
	public void updateStudentById(Long sid, Long contact_number) {
		StudentDto student = getStudentById1(sid);
		if(student!=null){
			student.setContactNumber(contact_number);
		Configuration configuration= new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(student);
		transaction.commit();
		System.out.println("update operation successful.");
		return;
	}
	System.out.println("update operation successful.");
}
	 public void deleteStudentById(Long sid, Long contact_number)
	 {
		 StudentDto student = getStudentById1(sid);
		 if(student!=null){
			student.setContactNumber(contact_number);
			Configuration configuration= new Configuration();
			configuration.configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.delete(student);
			transaction.commit();
			System.out.println("delete operation successful.");
			return;
		}
	 }
}

