package com.oehm5.basics.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "movie")
public class Movie implements Serializable{

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "movie_auto", strategy = "increment")
	@GeneratedValue(generator = "movie_auto")
	private Long id;
	
	@Column(name = "moovie_name")
	private String movieName;
	
	@Column(name = "release_date")
	private Date releaseDate;
	
	@Column(name = "rating")
	private String rating;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name = "m_a_common",
	joinColumns = {  @JoinColumn(name="m_id") },
	inverseJoinColumns = { @JoinColumn (name = "a_id") }
	)
	private List<Actor> list;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public List<Actor> getList() {
		return list;
	}

	public void setList(List<Actor> list) {
		this.list = list;
	}
	
	
}
