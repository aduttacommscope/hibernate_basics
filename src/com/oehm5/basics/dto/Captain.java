package com.oehm5.basics.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "captain")
public class Captain implements Serializable {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "cap_auto", strategy = "increment")
	@GeneratedValue(generator = "cap_auto")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "age")
	private Long age;

	@Column(name = "jersy_number")
	private String jersyNumber;

	@Column(name = "total_matches")
	private Long totalMatches;

	public Captain() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getJersyNumber() {
		return jersyNumber;
	}

	public void setJersyNumber(String jersyNumber) {
		this.jersyNumber = jersyNumber;
	}

	public Long getTotalMatches() {
		return totalMatches;
	}

	public void setTotalMatches(Long totalMatches) {
		this.totalMatches = totalMatches;
	}

}
