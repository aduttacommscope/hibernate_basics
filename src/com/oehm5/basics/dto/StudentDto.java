package com.oehm5.basics.dto;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

	@Entity
	@Table(name= "studentdetails")

	public class StudentDto implements Serializable {
		@Id 
		@Column(name = "sid")
		@GenericGenerator(name = "std_auto",parameters = @Parameter(name = "prefix", value = "student"), strategy = "com.oehm5.basics.util.MyCustomGenerator")
		@GeneratedValue(generator = "std_auto")
		private Long sid;
		@Column(name = "name")
		private String name;
		@Column(name = "Branch")
		private String Branch;
		@Column(name = "marks")
		private int marks;
		@Column(name = "ContactNumber")
		private long contactNumber;

		public StudentDto() {

		}

		public Long getSid() {
			return sid;
		}

		public void setSid(Long sid) {
			this.sid = sid;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBranch() {
			return Branch;
		}

		public void setBranch(String branch) {
			Branch = branch;
		}

		public int getMarks() {
			return marks;
		}

		public void setMarks(int marks) {
			this.marks = marks;
		}

		public void setContactNumber(long contactNumber) {
			this.contactNumber = contactNumber;
		}
		
		public void getContactNumber(long contactNumber) {
			this.contactNumber=contactNumber;
		}
		
		@Override
		public String toString() {
			return "StudentDto [sid=" + sid + ", name=" + name + ", branch=" + Branch + ", contactNumber=" + contactNumber
					+ ", marks=" + marks + "]";
		}

	}

