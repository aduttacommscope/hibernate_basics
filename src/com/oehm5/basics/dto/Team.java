package com.oehm5.basics.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "team")

public class Team implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "team_auto", strategy = "increment")
	@GeneratedValue(generator = "team_auto")
	private Long id;
	
	@Column(name = "country_name")
	private String countryName;
	
	@Column(name = "team_type")
	private String teamType;
	
	@Column(name = "size")
	private Long size;
	
	@Column(name = "rank")
	private String rank;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "f_id")
	private Captain captain;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getTeamType() {
		return teamType;
	}

	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Captain getCaptain() {
		return captain;
	}

	public void setCaptain(Captain captain) {
		this.captain = captain;
	}

}
