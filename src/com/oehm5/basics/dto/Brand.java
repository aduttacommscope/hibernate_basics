package com.oehm5.basics.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "brand")
public class Brand implements Serializable {
     
	@Id
		@Column(name = "id")
		//@GenericGenerator(name = "brand_auto", strategy = "increment")
	@GenericGenerator(name = "brand_auto",parameters = @Parameter(name = "prefix", value = "brand"), strategy = "increment")

		@GeneratedValue(generator = "brand_auto")
		private Long id;
		
		@Column(name = "name")
		private String name;
		
		@Column(name = "type")
		private String type;
		
		@Column(name = "year_of_establishment")
		private String yearOfEstablishment;
		
		@Column(name = "ambasdor")
		private String ambasdor;
		
		@OneToMany(cascade = CascadeType.ALL)
		@JoinColumn(name = "b_id")
		private List<Model> models;

		public Brand() {
			// TODO Auto-generated constructor stub
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getYearOfEstablishment() {
			return yearOfEstablishment;
		}

		public void setYearOfEstablishment(String yearOfEstablishment) {
			this.yearOfEstablishment = yearOfEstablishment;
		}

		public String getAmbasdor() {
			return ambasdor;
		}

		public void setAmbasdor(String ambasdor) {
			this.ambasdor = ambasdor;
		}

		public List<Model> getModels() {
			return models;
		}

		public void setModels(List<Model> models) {
			this.models = models;
		}
		
		
	}

