package com.oehm5.basics.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.oehm5.basics.dto.Model;

@Entity
@Table(name = "brand_mto")
public class Brand_Mto implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "brand_auto", strategy = "increment")

	@GeneratedValue(generator = "brand_auto")
	private Long id;
	
	@Column(name = "name")
	private String name;
	@Column(name = "type")
	private String type;
	@Column(name = "year_of_estb")
	private String year_of_estb;
	@Column(name = "ambassador")
	private String ambassador;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "b_id_mto")
	private List<Model> models;
	public Brand_Mto()
	{
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getYear_of_estb() {
		return year_of_estb;
	}

	public void setYear_of_estb(String year_of_estb) {
		this.year_of_estb = year_of_estb;
	}

	public String getAmbassador() {
		return ambassador;
	}

	public void setAmbassador(String ambassador) {
		this.ambassador = ambassador;
	}
	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}
	
}
