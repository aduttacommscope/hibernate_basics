package com.oehm5.basics.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "actor")

public class Actor implements Serializable {
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "actor_auto", strategy = "increment")
	@GeneratedValue(generator = "actor_auto")
	private Long id;
	
	@Column(name = "actor_name")
	private String actorName;
	
	@Column(name = "age")
	private Long age;
	
	@ManyToMany
	private List<Movie> list;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActorName() {
		return actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public List<Movie> getList() {
		return list;
	}

	public void setList(List<Movie> list) {
		this.list = list;
	}

}
